package com.library.loginservice.service.feign;

import com.library.loginservice.dto.GetNisnResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-management-service", url = "${user.management.service.url}")
public interface UserManagementServiceClient {

    @GetMapping("/api/students/findByNisn")
    GetNisnResponse getStudentByNisn(@RequestParam("nisn") String nisn);
}


