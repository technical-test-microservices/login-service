package com.library.loginservice.service;


import com.library.loginservice.domain.User;
import com.library.loginservice.dto.GetNisnResponse;
import com.library.loginservice.dto.UserRequest;
import com.library.loginservice.exception.CustomBadRequestException;
import com.library.loginservice.exception.CustomNotFoundException;
import com.library.loginservice.exception.GeneralBodyResponse;
import com.library.loginservice.service.feign.UserManagementServiceClient;
import com.library.loginservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserManagementServiceClient userManagementServiceClient;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserManagementServiceClient userManagementServiceClient) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userManagementServiceClient = userManagementServiceClient;
    }

    public GeneralBodyResponse checkNisn(String nisn) {
        GetNisnResponse checkNisn = userManagementServiceClient.getStudentByNisn(nisn);
        if (checkNisn.getCode() != 200){
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "NISN is not registered, please contact the admin to register or activate it");
        }

        User user = userRepository.findByNisn(nisn);
        if (user == null) {
            return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "NISN " + nisn + " is not registered, please continue your registration", null);
        } else {
            if (!user.isEnabled() && (user.getUsername() == null || user.getPassword() == null)) {
                return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "NISN " + nisn + " is registered but not activated, please continue your registration", null);
            } else {
                return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "NISN " + nisn + " is registered and activated, you can continue to create an account", null);
            }
        }
    }

    public GeneralBodyResponse createUser(UserRequest request) {
        GetNisnResponse checkNisn = userManagementServiceClient.getStudentByNisn(request.getNisn());
        if (checkNisn.getCode() != 200){
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "NISN not found");
        }

        User user = new User();
        user.setNisn(checkNisn.getData().getNisn());
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setEnabled(true);
        user.setCreatedAt(new Date());
        user.setRoles("ROLE_STUDENT");
        userRepository.save(user);

        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "User with NISN " + user.getNisn() + " has been created successfully", null);
    }


}
