package com.library.loginservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GetNisnResponse {
    private int code;
    private String status;
    private String message;
    private StudentResponse data;
}
