package com.library.loginservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentResponse {
    private String name;
    private String nisn;
}