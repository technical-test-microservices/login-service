package com.library.loginservice.repository;

import com.library.loginservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByNisn (String nisn);
    User findByUsernameAndEnabledTrue(String username);

}
