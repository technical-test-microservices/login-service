package com.library.loginservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CustomNotFoundException extends RuntimeException {
    private final int code;
    private final String status;
    private final String message;

    public CustomNotFoundException(int code, String status, String message) {
        super();

        this.code = code;
        this.status = status;
        this.message = message;
    }

}
