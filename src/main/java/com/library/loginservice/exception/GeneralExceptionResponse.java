package com.library.loginservice.exception;

import java.io.Serializable;

public class GeneralExceptionResponse implements Serializable {
    private int code;
    private String status;
    private String message;

    public GeneralExceptionResponse(int code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
