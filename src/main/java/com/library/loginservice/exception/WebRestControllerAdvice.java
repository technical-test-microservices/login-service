package com.library.loginservice.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class WebRestControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(CustomBadRequestException.class)
    public ResponseEntity<GeneralExceptionResponse> handleBadRequest(CustomBadRequestException exception) {
        GeneralExceptionResponse generalExceptionResponse = new GeneralExceptionResponse(exception.getCode(), exception.getStatus(), exception.getMessage());
        return ResponseEntity
            .status(exception.getCode())
            .body(generalExceptionResponse);
    }

    @ExceptionHandler(CustomNotFoundException.class)
    public ResponseEntity<GeneralExceptionResponse> handleNotFound(CustomNotFoundException exception) {
        GeneralExceptionResponse generalExceptionResponse = new GeneralExceptionResponse(exception.getCode(), exception.getStatus(), exception.getMessage());
        return ResponseEntity
            .status(exception.getCode())
            .body(generalExceptionResponse);
    }


}
