package com.library.loginservice.controller;


import com.library.loginservice.dto.UserRequest;
import com.library.loginservice.exception.GeneralBodyResponse;
import com.library.loginservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/chekNisn")
    public ResponseEntity<?> checkNisn(@RequestParam String nisn){
        GeneralBodyResponse response = userService.checkNisn(nisn);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody UserRequest request) {
        GeneralBodyResponse response = userService.createUser(request);
        return ResponseEntity
                .ok()
                .body(response);
    }
}
